#encoding: utf-8
namespace :db do
  desc "Publish posts"
  task :publish_posts => :environment do
    Public.scoped.each do |public|
      next_post = public.next_post
      next_post.publish! if next_post.present?
      sleep 15
    end
  end
end