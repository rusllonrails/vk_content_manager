class Post < ActiveRecord::Base
  attr_accessible \
    :vk_post_id,
    :public_id,
    :group_id,
    :photo,
    :text,
    :remote_photo_url,
    :publish_date,
    :post_date_time

  belongs_to :public
  belongs_to :group

  has_many :scheduled_posts

  mount_uploader :photo, PostPhotoUploader

  after_create :add_to_publish

  private

  def add_to_publish
    schedule = public.schedules.where(publish_date: publish_date).first

    if schedule.blank?
      schedule = public.schedules.create!(publish_date: publish_date)
    end

    schedule.scheduled_posts.create!(
      :post_date_time => post_date_time,
      :post_id => id,
      :published => false
    )
  end
end
