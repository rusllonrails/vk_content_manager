#encoding: utf-8

class Admin < ActiveRecord::Base

  devise \
    :database_authenticatable,
    :recoverable,
    :rememberable,
    :trackable,
    :validatable

  attr_accessible \
    :email,
    :password,
    :password_confirmation,
    :remember_me,
    :avatar

  mount_uploader :avatar, AdminAvatarUploader
end
