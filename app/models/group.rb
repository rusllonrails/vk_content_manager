#encoding: utf-8

class Group < ActiveRecord::Base

  TARGET_URL = "https://oauth.vk.com/authorize?client_id=3409136&scope=friends,groups,messages,offline,stats,wall,notify&redirect_uri=http://oauth.vk.com/blank.html&display=wap&response_type=token"
  VK_API_BASE_URL = "https://api.vk.com/method/"

  attr_accessible \
    :avatar,
    :name,
    :short_name,
    :vk_id,
    :gid,
    :closed

  has_many :posts
  has_many :people, class_name: 'InvitedPerson'

  mount_uploader :avatar, GroupAvatarUploader

  after_create :set_vk_data

  validates :vk_id, :uniqueness => true

  def vk_get_send_group_stats_request
    method = "groups.getMembers"
    group_id = '48529852'
    query_ops = "gid=#{group_id}"

    access_token = Account.first.access_token
    target_url = "#{VK_API_BASE_URL}#{method}?#{query_ops}&access_token=#{access_token}"

     # puts "[RESULT URL] target_url: #{target_url}"

    RestClient.get(target_url)
  end

  def vk_get_group_stats
    response = JSON.parse(vk_get_send_group_stats_request.body)['response']
    count, users_list = response['count'], response['users']

     # puts "[RESULT URL] count: #{count}"
     # puts "[RESULT URL] users_list: #{users_list}"

    [ count, users_list ]
  end

  def vk_is_member_of_group?(user_id, group_id)
    method = "groups.isMember"
    query_ops = "gid=#{group_id}&uid=#{user_id}"

    access_token = Account.first.access_token
    target_url = "#{VK_API_BASE_URL}#{method}?#{query_ops}&access_token=#{access_token}"

     # puts "[RESULT URL] target_url: #{target_url}"

    response = JSON.parse(RestClient.get(target_url).body)['response']
    response.to_s == '1'
  end

  def vk_wall_get_content_for_group
    method = "wall.get"
    query_ops = "owner_id=-#{self.gid}&count=#{10}"

    access_token = Account.first.access_token
    target_url = "#{VK_API_BASE_URL}#{method}?#{query_ops}&access_token=#{access_token}"

     # puts "[RESULT URL] target_url: #{target_url}"

    resp = RestClient.get(target_url)

     # puts "[RESULT URL] resp: #{resp.inspect}"
     Rails.logger.info "----------------[RESULT URL] resp: #{resp.inspect}"

    JSON.parse(resp.body)['response']
  end

  def vk_parse_last_ten_posts_from_group
    html_posts = []

    Rails.logger.info "----------ID-----------------#{self.id}"

    posts = vk_wall_get_content_for_group[1..-1]

    posts.each do |post|
       # puts "[RESULT] post: #{post}"

      if post['date'].present?
        date = Time.at(post['date'].to_i).in_time_zone
        text = post['text']
        post_id = "#{self.vk_id}_#{post['id']}"

        attach = post['attachments']
        if attach.present? && attach[0].present? && attach[0]['photo'].present?
          photo_ops = attach[0]['photo']

          big_photo = photo_ops['src_xbig'] if photo_ops['src_xbig'].present?
          big_photo = photo_ops['src_big'] if big_photo.blank? && photo_ops['src_big'].present?
          small_photo = photo_ops['src_small'] if photo_ops['src_small'].present?
        end

        parsed_post = {
          date: date,
          post_id: post_id
        }

        parsed_post[:text] = text
        parsed_post[:photo] = {
            big: big_photo,
            small: small_photo
        } if big_photo.present?

        html_posts << parsed_post
      end
    end

    html_posts
  end


  ############################################
  # Get people                               #
  ############################################

  def get_people
    method = "groups.getMembers"

    ops = {
      gid: gid,
      count: 1000,
      offset: 0,
      sort: 'id_asc'
    }.to_param

    puts "[RESULT URL] ops: #{ops}"

    access_token = Account.first.access_token

    puts "[RESULT URL] access_token: #{access_token}"

    target_url = "#{Public::VK_API_BASE_URL}#{method}?#{ops}&access_token=#{access_token}"

    puts "[RESULT URL] target_url: #{target_url}"

    uri = URI.encode(target_url.strip)

    resp = RestClient.get(uri)

    puts "[RESULT URL] JSON.parse(resp.body): #{JSON.parse(resp.body)}"

    response = JSON.parse(resp.body)['response']

    count_of_members = response['count'].to_i

    response['users'].each do |user_id|

      puts "[RESULT URL] user_id: #{user_id}"

      is_member = is_member?(user_id)
      in_black_list = false

      people.create!(
        vk_id: user_id,
        invited: false,
        in_black_list: in_black_list,
        is_member: is_member        
      ) unless InvitedPerson.find_by_vk_id(user_id).present?
    end

    count_of_requests = count_of_members / 1000

    count_of_members.times do |i|

      sleep 0.1

      puts "[RESULT URL] count_of_members: #{i}"

      method = "groups.getMembers"

      ops = {
        gid: gid,
        count: 1000,
        offset: 1000 * i,
        sort: 'id_asc'
      }.to_param

      puts "[RESULT URL] ops: #{ops}"

      access_token = Account.first.access_token

      puts "[RESULT URL] access_token: #{access_token}"

      target_url = "#{Public::VK_API_BASE_URL}#{method}?#{ops}&access_token=#{access_token}"

      puts "[RESULT URL] target_url: #{target_url}"

      uri = URI.encode(target_url.strip)

      resp = RestClient.get(uri)

      puts "[RESULT URL] JSON.parse(resp.body): #{JSON.parse(resp.body)}"

      response = JSON.parse(resp.body)['response']

      count_of_members = response['count'].to_i

      response['users'].each do |user_id|

        puts "[RESULT URL] user_id: #{user_id}"

        is_member = is_member?(user_id)
        in_black_list = false

        people.create!(
          vk_id: user_id,
          invited: false,
          in_black_list: in_black_list,
          is_member: is_member        
        ) unless InvitedPerson.find_by_vk_id(user_id).present?
      end
    end
  end

  def is_member?(user_id)
    sleep 0.1

    method = "groups.isMember"

    ops = {
      gid: Public.first.vk_id,
      uid: user_id
    }.to_param

    puts "[RESULT URL] ops: #{ops}"

    access_token = Account.first.access_token

    puts "[RESULT URL] access_token: #{access_token}"

    target_url = "#{Public::VK_API_BASE_URL}#{method}?#{ops}&access_token=#{access_token}"

    puts "[RESULT URL] target_url: #{target_url}"

    uri = URI.encode(target_url.strip)

    resp = RestClient.get(uri)

    puts "[RESULT URL] JSON.parse(resp.body): #{JSON.parse(resp.body)}"

    response = JSON.parse(resp.body)['response'][0]

    response.to_i == 1 ? true : false
  end

  ############################################
  # Invite people                            #
  ############################################

  def invite_people
    Account.update_all(invite_counter: 0)

    prepare_people.each do |person|
      account = Account.next

      template = InviteTemplate.next

      method = "messages.send"

      invited_person = person

      access_token = account.access_token

      ops = {
        uid: invited_person[0],
        title: template.title + " #{invited_person[1]}",
        message: ActionView::Base.full_sanitizer.sanitize(template.content).gsub("&nbsp;", ""),
        content_type: 'text/plain',
        attachment: "photo207657525_302085012"
      }

      puts "[RESULT URL] ops: #{ops}"

      target_url = "#{Public::VK_API_BASE_URL}#{method}?access_token=#{access_token}"

      puts "[RESULT URL] target_url: #{target_url}"

      uri = URI.encode(target_url.strip)

      resp = RestClient.post target_url, ops

      result = JSON.parse(resp.body)

      puts "[RESULT URL] JSON.parse(resp.body): #{result}"

      inv_person = InvitedPerson.find_by_vk_id(invited_person[0])

      if result['error'].present? 
        code = result['error']['error_code']

        inv_person.update_attributes(
          template_id: template.id,
          public_id: 4,
          error_code: code
        )
      else
        inv_person.update_attributes(
          template_id: template.id,
          public_id: 4
        )
      end

      template.increment_counter!
      account.increment_counter!
      inv_person.mark_as_invited!

      sleep 1
    end
  end

  def prepare_people
    real_count = 0
    access_token = Account.not_blocked.first.access_token
    pr_people = []

    person_id = InvitedPerson.next.id

    while real_count < (Account.not_blocked.count * 20) do
      person = InvitedPerson.not_invited.in_target_category.find_by_id(person_id)

      if person.present?
        data = get_person_data(person.vk_id, access_token)
        first_name, sex, can_write, relation, city, birth, online = data[0], data[1], data[2], data[3], data[4], data[5], data[6]

        puts "[RESULT URL] first_name: #{first_name}, sex: #{sex}, can_write: #{can_write}, relation: #{relation}, city: #{city}, birth: #{birth}, online: #{online}"

        success = city.to_i == 60 && sex.to_i == 1 && can_write.to_i == 1 && (birth.present? && birth.split('.')[2].to_i < 1994)

        if success
          if online.to_i == 0
            puts "[RESULT URL] success: #{success.present?}"
            pr_people << [ person.vk_id, first_name ]
            real_count += 1 
          end
        else
          person.update_attribute(:not_in_target_category, true)
        end
      end 
       
      person_id += 1

      puts "[RESULT URL] real_count: #{real_count}"

      sleep 0.3
    end

    puts "[RESULT URL] real_count: #{real_count}, pr_people: #{pr_people}"

    pr_people
  end

  def get_person_data(person_vk_id, access_token)
    method = "users.get"

    ops = {
      uids: person_vk_id
    }.to_param

    puts "[RESULT URL] ops: #{ops}"

    target_url = "#{Public::VK_API_BASE_URL}#{method}?#{ops}&access_token=#{access_token}&fields=first_name,sex,can_write_private_message,relation,city,online,bdate"

    puts "[RESULT URL] target_url: #{target_url}"

    uri = URI.encode(target_url.strip)

    resp = RestClient.get(uri)

    puts "[RESULT URL] JSON.parse(resp.body): #{JSON.parse(resp.body)}"

    response = JSON.parse(resp.body)['response'][0]

    puts "[RESULT URL] response: #{response}"

    [ response['first_name'], response['sex'], response['can_write_private_message'], response['relation'], response['city'], response['bdate'], response['online'] ]
  end

  ############################################
  # Invite people - Photo Attachment         #
  ############################################

  def save_base_photo
    access_token = Account.first.access_token
  end

  def get_wall_upload_server_url(access_token)
    g_w_u_server_url = "https://api.vk.com/method/photos.getWallUploadServer?gid=#{Public.last.vk_id}&access_token=#{access_token}"

     # puts "[RESULT URL] g_w_u_server_url: #{g_w_u_server_url.inspect}"

    resp = RestClient.get(g_w_u_server_url)

     # puts "[RESULT URL] g_w_u_server_url resp: #{resp.inspect}"

    response = JSON.parse(resp.body)['response']

     # puts "[RESULT URL] g_w_u_server_url response: #{response.inspect}"

     # puts "[RESULT URL] response['upload_url']: #{response['upload_url'].inspect}"

    response['upload_url']
  end

  def prepare_file
    post_image_url = InviteTemplate.first.image.url
    post_file = "#{Rails.root}/public" + post_image_url

    { photo: File.new(post_file, 'rb') }
  end

  def send_post_request_to_vk_upload_server(access_token)
    upload_url = get_wall_upload_server_url(access_token)

     # puts "[RESULT URL] upload_url: #{upload_url.inspect}"

    response = RestClient.post upload_url, prepare_file

     # puts "[RESULT URL] upload_url response: #{response.inspect}"

    parsed_response = JSON.parse(response.body)

     # puts "[RESULT URL] parsed_response: #{parsed_response.inspect}"

    parsed_response
  end

  def save_wall_photo(access_token)
    ops_hash = send_post_request_to_vk_upload_server(access_token)

    s_w_p_url = "https://api.vk.com/method/photos.saveWallPhoto?gid=#{Public.last.vk_id}&access_token=#{access_token}"

    ops = "&server=#{ops_hash['server']}&photo=#{ops_hash['photo']}&hash=#{ops_hash['hash']}"

    ops = {
      server: ops_hash['server'],
      photo: ops_hash['photo'],
      hash: ops_hash['hash']
    }

    # s_w_p_url += ops

     # puts "[RESULT URL] s_w_p_url: #{s_w_p_url.inspect}"

    response = JSON.parse(RestClient.post(s_w_p_url, ops).body)

     # puts "[RESULT URL] upload_url response: #{response.inspect}"

    response['response'][0]['id']
  end

  ############################################
  # Invite people - make likes and reposts   #
  ############################################

  def get_posts
    method = "wall.get"

    access_token = Account.not_blocked.first.access_token

    target_url = "#{Public::VK_API_BASE_URL}#{method}?access_token=#{access_token}&owner_id=-#{Public.first.vk_id}&count=15"

    puts "[RESULT URL] target_url: #{target_url}"

    uri = URI.encode(target_url.strip)

    resp = RestClient.get(uri)

    puts "[RESULT URL] JSON.parse(resp.body): #{JSON.parse(resp.body)}"

    response = JSON.parse(resp.body)['response'][1..-1]

    puts "[RESULT URL] response: #{response}"

    response.map { |r| r['id'] }
  end

  def check_if_licked?(post_id, vk_user_id)
    method = "likes.isLiked"

    access_token = Account.not_blocked.first.access_token

    target_url = "#{Public::VK_API_BASE_URL}#{method}?access_token=#{access_token}&item_id=#{post_id}&user_id=#{vk_user_id}&type=post"

    puts "[RESULT URL] target_url: #{target_url}"

    uri = URI.encode(target_url.strip)

    resp = RestClient.get(uri)

    puts "[RESULT URL] JSON.parse(resp.body): #{JSON.parse(resp.body)}"

    response = JSON.parse(resp.body)['response']

    puts "[RESULT URL] response: #{response}"

    response.to_s == '1'
  end

  def add_like(post_id, vk_user_id, access_token)
    method = "wall.addLike"

    target_url = "#{Public::VK_API_BASE_URL}#{method}?access_token=#{access_token}&post_id=#{post_id}&owner_id=-#{Public.first.vk_id}&repost=1"

    puts "[RESULT URL] target_url: #{target_url}"

    uri = URI.encode(target_url.strip)

    resp = RestClient.get(uri)

    puts "[RESULT URL] JSON.parse(resp.body): #{JSON.parse(resp.body)}"

    response = JSON.parse(resp.body)['response']

    puts "[RESULT URL] response: #{response}"
  end

  def add_likes
    get_posts.each do |post_id|
      Account.not_blocked.each do |account|
        sleep 0.2
        unless check_if_licked?(post_id, account.vk_id)
          sleep 0.2
          add_like(post_id, account.vk_id, account.access_token)
          sleep 0.2
        end
      end
    end
  end

  private

  def set_vk_data
  	method = "groups.getById"
    group_id = vk_id
    ops = "gid=#{group_id}"

    access_token = Account.first.access_token
    target_url = "#{VK_API_BASE_URL}#{method}?#{ops}&access_token=#{access_token}"

     # puts "[RESULT URL] target_url: #{target_url}"

    response = JSON.parse(RestClient.get(target_url).body)['response'][0]

     # puts "[RESULT URL] response: #{response.inspect}"

    self.gid = response['gid']
    self.name = response['name']
    self.short_name = response['screen_name']
    self.closed = response['is_closed'] == 0 ? true : false
    self.remote_avatar_url = response['photo_big']

    save!
  end
end
