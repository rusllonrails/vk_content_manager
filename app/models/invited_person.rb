class InvitedPerson < ActiveRecord::Base
  attr_accessible \
    :vk_id,
    :public_id,
    :template_id,
    :template,
    :public,
    :group,
    :group_id,
    :invited,
    :in_black_list,
    :is_member,
    :error_code,
    :not_in_target_category

  belongs_to :group
  belongs_to :public
  belongs_to :template

  scope :invited, where(invited: true)
  scope :not_invited, where(invited: false)
  scope :clear_invited, invited.where("error_code IS NULL")
  scope :in_target_category, where(not_in_target_category: false)
  scope :not_in_target_category, where(not_in_target_category: true)

  def mark_as_invited!
    update_attribute(:invited, true)
  end

  class << self
    def next
      not_invited.in_target_category.first
    end
  end
end