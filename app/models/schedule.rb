class Schedule < ActiveRecord::Base
  POSTING_DATE_TIMES = [
    '08:00',
    '09:00',
    '10:00',
    '11:00',
    '12:00',
    '13:00',
    '14:00',
    '15:00',
    '16:00',
    '17:00',
    '18:00',
    '19:00',
    '20:00',
    '21:00',
    '22:00',
    '23:00'
  ]

  attr_accessible :public_id, :publish_date

  belongs_to :public

  has_many :scheduled_posts

  scope :between_dates, lambda { |start_date, end_date|
    where("publish_date between (?) AND (?)", start_date, end_date) }
end
