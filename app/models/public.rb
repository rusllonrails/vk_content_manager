#encoding: utf-8

class Public < ActiveRecord::Base

  LOGIN_URL = 'https://vk.com'
  SCOPES = "notify,friends,photos,audio,video,docs,notes,pages,status,offers,questions,wall,groups,messages,notifications,stats,ads,offline"
  TARGET_URL = "https://oauth.vk.com/authorize?client_id=3409136&scope=#{SCOPES}&redirect_uri=http://oauth.vk.com/blank.html&display=wap&response_type=token"
  VK_API_BASE_URL = "https://api.vk.com/method/"

  attr_accessible \
    :avatar,
    :account_id,
    :closed,
    :name,
    :short_name,
    :vk_id

  belongs_to :account

  has_many :schedules

  has_many :posts

  mount_uploader :avatar, GroupAvatarUploader

  after_create :set_vk_data

  validates :vk_id, :uniqueness => true

  def vk_get_send_group_stats_request
    method = "groups.getMembers"
    group_id = vk_id
    query_ops = "gid=#{group_id}"

    access_token = Account.first.access_token
    target_url = "#{VK_API_BASE_URL}#{method}?#{query_ops}&access_token=#{access_token}"

     # puts "[RESULT URL] target_url: #{target_url}"

    RestClient.get(target_url)
  end

  def vk_get_group_stats
    response = JSON.parse(vk_get_send_group_stats_request.body)['response']
    count, users_list = response['count'], response['users']

     # puts "[RESULT URL] count: #{count}"
     # puts "[RESULT URL] users_list: #{users_list}"

    [ count, users_list ]
  end

  def today_schedule
    schedules.between_dates(Time.zone.now.midnight, Time.zone.now.midnight + 1.day).first
  end

  def next_post
    #today_schedule.scheduled_posts.order('post_date_time ASC').first
    ScheduledPost.order('post_date_time ASC').first
  end

  private

  def set_vk_data
  	method = "groups.getById"
    group_id = vk_id
    ops = "gid=#{group_id}"

    access_token = Account.first.access_token
    target_url = "#{VK_API_BASE_URL}#{method}?#{ops}&access_token=#{access_token}"

     # puts "[RESULT URL] target_url: #{target_url}"

    response = JSON.parse(RestClient.get(target_url).body)['response'][0]

     # puts "[RESULT URL] response: #{response.inspect}"

    self.name = response['name']
    self.short_name = response['screen_name']
    self.closed = response['is_closed'] == 0 ? true : false
    self.remote_avatar_url = response['photo_big']

    save!
  end
end
