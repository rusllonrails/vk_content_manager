class ScheduledPost < ActiveRecord::Base
  attr_accessible \
    :post_date_time,
    :post_id,
    :published,
    :schedule_id,
    :vk_post_id

  belongs_to :schedule
  belongs_to :post

  scope :unpublished, where(published: false)

  def public
    post.public
  end

  def publish!
    publish_to_vk!
  end

  def get_wall_upload_server_url(access_token)
    g_w_u_server_url = "https://api.vk.com/method/photos.getWallUploadServer?gid=#{public.vk_id}&access_token=#{access_token}"

     # puts "[RESULT URL] g_w_u_server_url: #{g_w_u_server_url.inspect}"

    resp = RestClient.get(g_w_u_server_url)

     # puts "[RESULT URL] g_w_u_server_url resp: #{resp.inspect}"

    response = JSON.parse(resp.body)['response']

     # puts "[RESULT URL] g_w_u_server_url response: #{response.inspect}"

     # puts "[RESULT URL] response['upload_url']: #{response['upload_url'].inspect}"

    response['upload_url']
  end

  def prepare_file
    post_image_url = post.photo.url
    post_image_file_name = post.photo.file.filename

    system("cd #{Rails.root}/public/tmp_uploads/ && wget #{post_image_url}")
    post_file = "#{Rails.root}/public/tmp_uploads/" + post_image_file_name

    { photo: File.new(post_file, 'rb') }
  end

  def send_post_request_to_vk_upload_server(access_token)
    upload_url = get_wall_upload_server_url(access_token)

     # puts "[RESULT URL] upload_url: #{upload_url.inspect}"

    response = RestClient.post upload_url, prepare_file

     # puts "[RESULT URL] upload_url response: #{response.inspect}"

    parsed_response = JSON.parse(response.body)

     # puts "[RESULT URL] parsed_response: #{parsed_response.inspect}"

    parsed_response
  end

  def save_wall_photo(access_token)
    ops_hash = send_post_request_to_vk_upload_server(access_token)

    s_w_p_url = "https://api.vk.com/method/photos.saveWallPhoto?gid=#{public.vk_id}&access_token=#{access_token}"

    ops = "&server=#{ops_hash['server']}&photo=#{ops_hash['photo']}&hash=#{ops_hash['hash']}"

    ops = {
      server: ops_hash['server'],
      photo: ops_hash['photo'],
      hash: ops_hash['hash']
    }

    # s_w_p_url += ops

     # puts "[RESULT URL] s_w_p_url: #{s_w_p_url.inspect}"

    response = JSON.parse(RestClient.post(s_w_p_url, ops).body)

     # puts "[RESULT URL] upload_url response: #{response.inspect}"

    response['response'][0]['id']
  end

  def publish_to_vk!
    method = "wall.post"

    ops = "owner_id=-#{public.vk_id}&from_group=1"

    if post.text.present?
      message = "&message=#{post.text}"
      ops += message
    end

    access_token = Account.first.access_token

    if post.photo.present? && post.photo.file.present?
      prepared_photo = save_wall_photo(access_token)

      attachments = "&attachments=#{prepared_photo}"
      ops += attachments
    end

    target_url = "#{public.class::VK_API_BASE_URL}#{method}?#{ops}&access_token=#{access_token}"

     # puts "[RESULT URL] target_url: #{target_url}"

    uri = URI.encode(target_url.strip)

    resp = RestClient.get(uri)

    response = JSON.parse(resp.body)['response'][0]

     # puts "[RESULT URL] response: #{response.inspect}"

    self.post.destroy
    self.destroy
  end
end
