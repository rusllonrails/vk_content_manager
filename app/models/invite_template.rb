class InviteTemplate < ActiveRecord::Base

  set_table_name :templates

  attr_accessible :content, :image, :title, :invite_counter

  mount_uploader :image, InviteTemplateUploader

  def increment_counter!
  	update_attribute(:invite_counter, invite_counter + 1)
  end

  class << self
    def next
      where(invite_counter: minimum("invite_counter")).first
    end
  end
end
