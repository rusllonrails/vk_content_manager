#encoding: utf-8

class Account < ActiveRecord::Base

  LOGIN_URL = 'https://vk.com'
  SCOPES = "notify,friends,photos,audio,video,docs,notes,pages,status,offers,questions,wall,groups,messages,notifications,stats,ads,offline"
  TARGET_URL = "https://oauth.vk.com/authorize?client_id=3409136&scope=#{SCOPES}&redirect_uri=http://oauth.vk.com/blank.html&display=wap&response_type=token"
  VK_API_BASE_URL = "https://api.vk.com/method/"

  attr_accessible \
    :vk_id,
    :phone,
    :first_name,
    :last_name,
    :avatar,
    :blocked,
    :password,
    :access_token,
    :invite_counter,
    :unlock

  attr_accessor :unlock

  has_many :publics

  mount_uploader :avatar, AccountAvatarUploader

  validates :vk_id, :phone,
    :presence => true,
    :uniqueness => true

  after_create :add_vk_data
  after_update :add_vk_data, if: 'unlock.present?'

  scope :blocked, where(blocked: true)
  scope :not_blocked, where(blocked: false)

  def increment_counter!
    update_attribute(:invite_counter, invite_counter + 1)
  end

  def new_password
    SecureRandom.hex(4)
  end

  class << self
    def next
      not_blocked.where(invite_counter: not_blocked.minimum("invite_counter")).first
    end
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def vk_login
    client = Mechanize.new
    client.get(LOGIN_URL)
    form = client.page.forms.first

 # Rails.logger.info "[ LOGGER 1 ] phone: #{phone.inspect}"

    form.email = phone

 # Rails.logger.info "[ LOGGER 1 ] password: #{password.inspect}"

    form.pass = password
    form.submit

    client
  end

  def vk_get_perms_page
    client = vk_login
    client.get(TARGET_URL)

     # Rails.logger.info "[ LOGGER 1 - 3 ] client.page.forms.first: #{client.page.forms.first.inspect}"
    form = client.page.forms.first
    form.submit

    client
  end

  def vk_get_creds(client)
   # Rails.logger.info "[ LOGGER 2 - 1 ] client.page: #{client.page.inspect}"

   # Rails.logger.info "[ LOGGER 2 - 2 ] client.page.uri: #{client.page.uri.inspect}"

   # Rails.logger.info "[ LOGGER 2 - 3 ] client.page.uri.to_json: #{client.page.uri.to_json.inspect}"

    response_ops = JSON.parse(client.page.uri.to_json)
   # Rails.logger.info "[ LOGGER 2 - 4 ] response_ops: #{response_ops.inspect}"
    fragments = response_ops['fragment'].split('&')
    vk_parse_response(fragments)
  end

  def vk_parse_response(fragments)
    access_token, user_id, expires_in = nil, nil, nil

    fragments.each do |fragment|
      if fragment.include?('access_token')
        access_token = fragment.split('=')[1]
      elsif fragment.include?('user_id')
        user_id = fragment.split('=')[1]
      elsif fragment.include?('expires_in')
        expires_in = fragment.split('=')[1]
      end
    end

    [ access_token, user_id, expires_in ]
  end

  def locked?
    method = "users.get"
    target_url = "#{VK_API_BASE_URL}#{method}?access_token=#{access_token}&uids=#{vk_id}"

    puts "[RESULT URL] target_url: #{target_url}"

    uri = URI.encode(target_url.strip)

    resp = RestClient.get(uri)

    puts "[RESULT URL] JSON.parse(resp.body): #{JSON.parse(resp.body)}"

    response = JSON.parse(resp.body)
    response['error'].present? ? true : false
  end

  private

  def vk_user_details_request
    method = "users.get"
    uids = vk_id
    query_ops = "uids=#{uids}"
    client = vk_get_perms_page
    access_token = vk_get_creds(client).first
    target_url = "#{VK_API_BASE_URL}#{method}?#{query_ops}&access_token=#{access_token}"

    client.get(target_url)
  end

  def add_vk_data
    client = vk_get_perms_page
    access_token = vk_get_creds(client).first
    self.access_token = access_token

    method = "users.get"
    query_ops = "uids=#{vk_id}"
    fields = "first_name, last_name, photo_big"

    target_url = "#{VK_API_BASE_URL}#{method}?#{query_ops}&access_token=#{access_token}&fields=#{fields}"

    response = client.get(target_url)

    data = JSON.parse(response.body)['response'][0]

    self.first_name = data['first_name']
    self.last_name = data['last_name']
    self.remote_avatar_url = data['photo_big']

    self.save!
  end
end
