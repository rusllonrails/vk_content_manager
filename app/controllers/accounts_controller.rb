# encoding: utf-8

class AccountsController < ApplicationController

  before_filter :authenticate_admin!, :set_active_tab

  def index
    @accounts = Account.scoped
  end

  def show
    @account = Account.find(params[:id])
  end

  def new
    @account = Account.new
  end

  def create
    @account = Account.new(params[:account])

    if @account.save
      redirect_to accounts_url, notice: 'Паблик успешно добавлен!'
    else
      render action: "new"
    end
  end

  def destroy
    @account = Account.find(params[:id])
    @account.destroy

    redirect_to accounts_url
  end

  def show_unlock_popup
    @account = Account.find(params[:account_id])
  end

  def unlock
    @account = Account.find(params[:account_id])
    @account.update_attributes(password: params[:password], unlock: true)
  end

  private

  def set_active_tab
    @active_tab = 'accounts'
  end  
end
