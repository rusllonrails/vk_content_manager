# encoding: utf-8

class GroupsController < ApplicationController

  before_filter :authenticate_admin!, :set_active_tab

  def index
    @groups = Group.scoped
  end

  def show
    @group = Group.find(params[:id])
    @posts = [] # @group.vk_parse_last_ten_posts_from_group
  end

  def new
    @group = Group.new
  end

  def create
    @group = Group.new(params[:group])

    if @group.save
      redirect_to groups_url, notice: 'Группа успешно добавлена!'
    else
      render action: "new"
    end
  end

  def destroy
    @group = Group.find(params[:id])
    @group.destroy

    redirect_to groups_url
  end

  def get_people
    @group = Group.find(params[:group_id])
    @group.get_people

    redirect_to group_url(@group)
  end

  private

  def set_active_tab
    @active_tab = 'generators'
  end
end
