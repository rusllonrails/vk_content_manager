# encoding: utf-8

class PostsController < ApplicationController

  before_filter :authenticate_admin!, :set_active_tab

  def index
    @posts = []

    Group.scoped.limit(8).to_a.shuffle.each do |group|

      Rails.logger.info "--------SLEEEP TO 15 seconds------start---------"

      sleep 15

      Rails.logger.info "--------SLEEEP TO 15 seconds------end---------"
            
      g_posts = group.vk_parse_last_ten_posts_from_group

      if g_posts.present?
        g_posts.each do |g_post|
          g_post[:group_id] = group.id
          @posts << g_post
        end
      end

      Rails.logger.info "--------SLEEEP TO 15 seconds------start---------"

      sleep 15

      Rails.logger.info "--------SLEEEP TO 15 seconds------end---------"
    end

    @posts = @posts.shuffle
  end

  def show
    @post = Post.find(params[:id])
  end

  def new
    @post = Post.new
  end

  def edit
    @post = Post.find(params[:id])
  end

  def create
    p_year = params[:post]["publish_date(1i)"].to_i
    p_month = params[:post]["publish_date(2i)"].to_i
    p_day = params[:post]["publish_date(3i)"].to_i
    p_time = params[:post]["post_date_time"].split(':')
    p_hours = p_time[0].to_i
    p_minutes = p_time[1].to_i

    post_date_time = DateTime.civil(p_year, p_month, p_day, p_hours, p_minutes)

    @post = Post.new(params[:post].merge!(post_date_time: post_date_time))
    @post.save!
  end

  def update
    @post = Post.find(params[:id])

    if @post.update_attributes(params[:vk_post])
      redirect_to @post, notice: 'Vk post was successfully updated.'
    else
      render action: "edit"
    end
  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy

    redirect_to vk_posts_url
  end

  private

  def set_active_tab
    @active_tab = 'posts'
  end
end
