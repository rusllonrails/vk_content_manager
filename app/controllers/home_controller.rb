# encoding: utf-8

class HomeController < ApplicationController

  before_filter :authenticate_admin!, :set_active_tab

  def index
  end

  private

  def set_active_tab
    @active_tab = 'statistics'
  end
end
