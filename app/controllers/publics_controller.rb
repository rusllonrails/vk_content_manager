# encoding: utf-8

class PublicsController < ApplicationController

  before_filter :authenticate_admin!, :set_active_tab

  def index
    @publics = Public.scoped
  end

  def show
    @public = Public.find(params[:id])
  end

  def new
    @public = Public.new
  end

  def create
    @public = Public.new(params[:public])

    if @public.save
      redirect_to publics_url, notice: 'Паблик успешно добавлен!'
    else
      render action: "new"
    end
  end

  def destroy
    @public = Public.find(params[:id])
    @public.destroy

    redirect_to publics_url
  end

  private

  def set_active_tab
    @active_tab = 'publics'
  end  
end
