module ApplicationHelper
  def is_active_tab?(tab)
  	@active_tab == tab ? 'active' : ''
  end
end
