VkContentManager::Application.routes.draw do

  devise_for :admins

  resources :groups, except: [ :edit, :update ] do
    get :get_people
  end
  resources :publics, except: [ :edit, :update ]
  resources :posts
  resources :accounts do
  	get :show_unlock_popup
  	post :unlock
  end
  resources :invite_templates

  root to: 'accounts#index'
end
