require File.expand_path('../application', __FILE__)

cron_job_template = "/usr/local/bin/bash -l -c ':job'"

set :job_template, cron_job_template
set :output, File.expand_path("#{File.dirname __FILE__}/../log/cron_log.log")

every 1.minute do
  runner "LikeLera.run"
end
