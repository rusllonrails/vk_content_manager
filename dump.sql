-- MySQL dump 10.13  Distrib 5.5.29, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: vk_content_manager_dev
-- ------------------------------------------------------
-- Server version	5.5.29-0ubuntu0.12.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `blocked` tinyint(1) DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vk_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` VALUES (2,'79518930885','2013-03-14 05:45:57','2013-03-14 05:45:57',NULL,'Человек','И-Пароход','907GcqZxlm8.jpg','197940422','alkapone1985','22013cda288bc12d05bbbbf6352dd353f00c276b39d294f1be0d3d325f3a0aafefc1ff6de3a7ac08da758');
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_credentials`
--

DROP TABLE IF EXISTS `admin_credentials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_credentials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) DEFAULT NULL,
  `vk_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_credentials`
--

LOCK TABLES `admin_credentials` WRITE;
/*!40000 ALTER TABLE `admin_credentials` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_credentials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `encrypted_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_admins_on_email` (`email`),
  UNIQUE KEY `index_admins_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (1,'ror_developer@mail.ru','$2a$10$yUIMSpovlrSL8.7R/muDsux7WcPQ8erD/ZaMK7Nj9838dDCD35i7i',NULL,NULL,NULL,3,'2013-03-15 10:42:32','2013-03-14 17:19:30','127.0.0.1','127.0.0.1','2013-03-14 05:39:28','2013-03-15 10:42:32',NULL);
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `delayed_jobs`
--

DROP TABLE IF EXISTS `delayed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delayed_jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `priority` int(11) DEFAULT '0',
  `attempts` int(11) DEFAULT '0',
  `handler` text COLLATE utf8_unicode_ci,
  `last_error` text COLLATE utf8_unicode_ci,
  `run_at` datetime DEFAULT NULL,
  `locked_at` datetime DEFAULT NULL,
  `failed_at` datetime DEFAULT NULL,
  `locked_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `queue` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `delayed_jobs_priority` (`priority`,`run_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `delayed_jobs`
--

LOCK TABLES `delayed_jobs` WRITE;
/*!40000 ALTER TABLE `delayed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `delayed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vk_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `closed` tinyint(1) DEFAULT NULL,
  `gid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (2,'MDK','mudakoff','mudakoff','jYNGsIkciyk.jpg','2013-03-14 05:54:22','2013-03-14 05:54:22',1,10639516),(3,'БОРЩ','borsch','borsch','XwykoD3M8qg.jpg','2013-03-14 06:38:35','2013-03-14 06:38:35',1,460389),(4,'Ёбаный пиздец','fuck_humor','fuck_humor','d1HwOqfHOZc.jpg','2013-03-14 06:39:31','2013-03-14 06:39:31',1,12382740),(5,'О бля! - Где бля? - Вон бля! - О бля!','comics_troll_face','comics_troll_face','a_c98062f5.jpg','2013-03-14 06:39:43','2013-03-14 06:39:43',1,24099488),(6,'Ебаный стыд | +18 |','pizdato_0','pizdato_0','a_ea633887.jpg','2013-03-14 06:39:55','2013-03-14 06:39:55',1,31613023),(7,'УПОРОТЫЕ','uporoty_vk','uporoty_vk','mIeGcZpzb0s.jpg','2013-03-14 06:40:03','2013-03-14 06:40:03',1,38119975),(8,'MDK','mudachyo','mudachyo','IJpVrxQhyG8.jpg','2013-03-14 06:40:11','2013-03-14 06:40:11',1,23148107),(9,'Четкие приколы :D','ilikes','ilikes','bIIj57i9clY.jpg','2013-03-14 06:40:31','2013-03-14 06:40:31',1,23064236),(10,'Безудержное веселье :)','really_fun','really_fun','wXyfE9XMNhM.jpg','2013-03-14 06:40:39','2013-03-14 06:40:39',1,43503460),(11,'В шоке! 18+','i_shok','i_shok','a_6fe6671c.jpg','2013-03-14 06:40:46','2013-03-14 06:40:46',1,31541440),(12,'MozG off','crazy.group','crazy.group','bSTjnsbSBtI.jpg','2013-03-14 06:40:58','2013-03-14 06:40:58',1,40684908),(14,'РжУ НеМАгУ ٩(-̮̮̃-̃)۶ 18+','club39391003','39391003','a_d2023f90.jpg','2013-03-14 06:41:19','2013-03-14 06:41:19',1,39391003),(15,'Оптимист','optimist.group','optimist.group','a_8ce7f74b.jpg','2013-03-14 06:45:00','2013-03-14 06:45:00',1,36912942),(16,'Злые','zloygad','zloygad','91zWoRDj1H4.jpg','2013-03-14 06:45:12','2013-03-14 06:45:12',1,39776802),(17,'Чёрный юмор','black.humor','black.humor','mvYVVRjVTbY.jpg','2013-03-14 06:45:32','2013-03-14 06:45:32',1,40835481),(18,'Эгоист','egoist.group','egoist.group','a_4e9ec0ac.jpg','2013-03-14 06:45:46','2013-03-14 06:45:46',1,36731992),(19,'ПОЗИТИВ ツ','pozitiv','pozitiv','why88j_SA40.jpg','2013-03-14 06:45:55','2013-03-14 06:45:55',1,11614),(20,'ПРИКОЛЫ','aqvamania','aqvamania','gv_EbnWSw54.jpg','2013-03-14 06:46:05','2013-03-14 06:46:05',1,20673510),(21,'Интеллектуальный юмор','cards.blog','cards.blog','a_66a7bb0c.jpg','2013-03-14 06:46:20','2013-03-14 06:46:20',1,28950133),(22,'FunWall - лучшие Приколы!','funwall_ru','funwall_ru','Egdb9hb-mzk.jpg','2013-03-14 06:46:45','2013-03-14 06:46:45',1,42110933),(23,'Всё включено!','all_rus','all_rus','9Ezlsfpod_w.jpg','2013-03-14 06:46:57','2013-03-14 06:46:57',1,42758242),(24,'ОСТРЯК','club49583292','49583292','WGRmPGuN-vk.jpg','2013-03-14 06:47:07','2013-03-14 06:47:07',1,49583292),(25,'DNIWE','ebadno','ebadno','Bf6EeBM_UuQ.jpg','2013-03-14 06:47:18','2013-03-14 06:47:18',1,42055093),(26,'FOX','foxvk','foxvk','-Ex-wdzP1Xk.jpg','2013-03-14 06:47:29','2013-03-14 06:47:29',1,44343962),(27,'Злой Разум','zloi.razum','zloi.razum','HkCDqy8hZg4.jpg','2013-03-14 06:47:38','2013-03-14 06:47:38',1,42818842),(28,'Просто коры :D','p_r_o_s_t_o__k_o_r_i','p_r_o_s_t_o__k_o_r_i','kIQ74JzrDVg.jpg','2013-03-14 06:47:49','2013-03-14 06:47:49',1,24260566),(29,'Приколы','angab','angab','jxKtdKq7kJQ.jpg','2013-03-14 06:48:02','2013-03-14 06:48:02',1,23413763),(30,'Улётные приколы','fangif','fangif','a_10090d54.jpg','2013-03-14 06:48:14','2013-03-14 06:48:14',1,30034017),(31,'Приколы','club41884929','41884929','a_b0901a76.jpg','2013-03-14 06:48:24','2013-03-14 06:48:24',1,41884929),(32,'Чёткие приколы :D','etopizdets','etopizdets','BAEYaspDdV0.jpg','2013-03-14 06:48:34','2013-03-14 06:48:34',1,33458888),(33,'ПРИКОЛЫ','humoristika','humoristika','a_095a6809.jpg','2013-03-14 06:48:44','2013-03-14 06:48:44',1,32586219),(34,'Анекдоты и приколы','best_humor_collection','best_humor_collection','a_160229e7.jpg','2013-03-14 06:48:53','2013-03-14 06:48:53',1,36712100),(35,'Корпорация Приколов','real_prikols','real_prikols','1cFD-exPnuI.jpg','2013-03-14 06:49:06','2013-03-14 06:49:06',1,41260105),(36,'В пизду моральность','pizes','pizes','a_ed8554c8.jpg','2013-03-14 06:52:07','2013-03-14 06:52:07',1,32755576),(37,'Like - Сумасшедшие приколы','flike','flike','b8rNBCPk2Y0.jpg','2013-03-14 06:52:58','2013-03-14 06:52:58',1,29435908),(38,'Нет, блять!','neblya','neblya','a_290b1972.jpg','2013-03-14 06:53:08','2013-03-14 06:53:08',1,34187905),(39,'Похуист','poh_blog','poh_blog','a_d530d9d5.jpg','2013-03-14 06:53:15','2013-03-14 06:53:15',1,36806776),(40,'• iFace','iface','iface','s0cXou90g9E.jpg','2013-03-14 06:53:23','2013-03-14 06:53:23',1,30277672);
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invited_people`
--

DROP TABLE IF EXISTS `invited_people`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invited_people` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vk_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invited_people`
--

LOCK TABLES `invited_people` WRITE;
/*!40000 ALTER TABLE `invited_people` DISABLE KEYS */;
/*!40000 ALTER TABLE `invited_people` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `public_id` int(11) DEFAULT NULL,
  `vk_post_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `post_date_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (74,'','m4bZQkrZvD4.jpg','2013-03-14 17:35:42','2013-03-14 17:35:42',11,1,'i_shok_36219','2013-03-13 20:00:00','2013-03-14 08:00:00'),(75,'Готов к свиданке','mB4MYSwVsyU.jpg','2013-03-14 17:36:03','2013-03-14 17:36:03',20,1,'aqvamania_3001','2013-03-13 20:00:00','2013-03-14 08:00:00'),(76,'','lpvTK1vQxU4.jpg','2013-03-14 17:36:12','2013-03-14 17:36:12',12,1,'crazy.group_598064','2013-03-13 20:00:00','2013-03-14 08:00:00'),(77,'','ZdnzFA-M7N8.jpg','2013-03-14 17:36:34','2013-03-14 17:36:34',9,1,'ilikes_101157','2013-03-13 20:00:00','2013-03-14 08:00:00'),(78,'','j2YXNDp03Xc.jpg','2013-03-14 17:37:31','2013-03-14 17:37:31',14,1,'39391003_20370','2013-03-13 20:00:00','2013-03-14 08:00:00'),(79,'','53NIDclygM8.jpg','2013-03-14 17:37:57','2013-03-14 17:37:57',14,1,'39391003_20381','2013-03-13 20:00:00','2013-03-14 08:00:00'),(80,'','i8IS-AwAXUg.jpg','2013-03-14 17:38:53','2013-03-14 17:38:53',20,1,'aqvamania_3006','2013-03-13 20:00:00','2013-03-14 08:00:00'),(81,'','Yy7NXE1LfY4.jpg','2013-03-14 17:39:50','2013-03-14 17:39:50',5,1,'comics_troll_face_262639','2013-03-13 20:00:00','2013-03-14 08:00:00'),(84,'','QxdoConzWmk.jpg','2013-03-14 17:40:59','2013-03-14 17:40:59',40,2,'iface_177484','2013-03-13 20:00:00','2013-03-14 08:00:00'),(85,'','pJEtfml2Pyc.jpg','2013-03-14 17:42:26','2013-03-14 17:42:26',10,3,'really_fun_1303','2013-03-13 20:00:00','2013-03-14 08:00:00'),(86,'Семейное фото)','WwtbfNQxA_A.jpg','2013-03-14 17:42:36','2013-03-14 17:42:36',20,2,'aqvamania_3000','2013-03-13 20:00:00','2013-03-14 08:00:00'),(87,'','aigk81DBwEQ.jpg','2013-03-14 17:42:54','2013-03-14 17:42:54',11,3,'i_shok_36218','2013-03-13 20:00:00','2013-03-14 08:00:00'),(88,'- Бабуль, а бабуль, дай 5 рублей.<br>- Чего?<br>- Дай 5 рублей!<br>- Чего?<br>- Дай 10 рублей!<br>- Ты ж 5 просил!?',NULL,'2013-03-14 17:43:13','2013-03-14 17:43:13',24,2,'49583292_1132','2013-03-13 20:00:00','2013-03-14 08:00:00'),(89,'Популярность требует большого труда! :D','8h-8FtR3p9k.jpg','2013-03-14 17:43:35','2013-03-14 17:43:35',19,2,'pozitiv_253807','2013-03-13 20:00:00','2013-03-14 08:00:00'),(90,'','NG_GpNRgYU8.jpg','2013-03-14 17:43:45','2013-03-14 17:43:45',19,3,'pozitiv_253802','2013-03-13 20:00:00','2013-03-14 08:00:00'),(91,'Самое главное — успеть лечь спать до того, как захочется жрать.',NULL,'2013-03-14 17:43:54','2013-03-14 17:43:54',24,1,'49583292_1136','2013-03-13 20:00:00','2013-03-14 08:00:00'),(92,'','0e8t6f7006k.jpg','2013-03-14 17:44:24','2013-03-14 17:44:24',10,3,'really_fun_1307','2013-03-13 20:00:00','2013-03-14 08:00:00'),(93,'','QAaYCXgF_do.jpg','2013-03-14 17:44:33','2013-03-14 17:44:33',8,3,'mudachyo_4457902','2013-03-13 20:00:00','2013-03-14 08:00:00'),(94,'','v0AFL-HkhP4.jpg','2013-03-14 17:44:41','2013-03-14 17:44:41',30,1,'fangif_174430','2013-03-13 20:00:00','2013-03-14 08:00:00'),(95,'','v0AFL-HkhP4.jpg','2013-03-14 17:50:00','2013-03-14 17:50:00',30,3,'fangif_174430','2013-03-13 20:00:00','2013-03-14 08:00:00'),(96,'','QAaYCXgF_do.jpg','2013-03-14 17:50:13','2013-03-14 17:50:13',8,2,'mudachyo_4457902','2013-03-13 20:00:00','2013-03-14 08:00:00'),(97,'','wZEhJqqC058.jpg','2013-03-14 17:50:54','2013-03-14 17:50:54',35,2,'real_prikols_8229','2013-03-13 20:00:00','2013-03-14 08:00:00'),(98,'','wZEhJqqC058.jpg','2013-03-14 17:51:05','2013-03-14 17:51:05',35,1,'real_prikols_8229','2013-03-13 20:00:00','2013-03-14 08:00:00'),(99,'','Krx3IdqKHxI.jpg','2013-03-14 17:51:15','2013-03-14 17:51:15',25,2,'ebadno_270336','2013-03-13 20:00:00','2013-03-14 08:00:00'),(100,'','c8kd5CM4xfk.jpg','2013-03-14 17:51:27','2013-03-14 17:51:27',20,3,'aqvamania_2999','2013-03-13 20:00:00','2013-03-14 08:00:00'),(101,'','G5-n0lIMQQA.jpg','2013-03-14 17:51:40','2013-03-14 17:51:40',26,1,'foxvk_9689','2013-03-13 20:00:00','2013-03-14 08:00:00'),(102,'','qjBu3hZknms.jpg','2013-03-14 17:52:47','2013-03-14 17:52:47',20,3,'aqvamania_3004','2013-03-13 20:00:00','2013-03-14 08:00:00'),(103,'','k715LaYPhrU.jpg','2013-03-14 17:53:28','2013-03-14 17:53:28',8,3,'mudachyo_4457894','2013-03-13 20:00:00','2013-03-14 08:00:00'),(104,'Тут что-то не так.','l4GTZ2Q1zQ0.jpg','2013-03-14 17:53:41','2013-03-14 17:53:41',9,2,'ilikes_101156','2013-03-13 20:00:00','2013-03-14 08:00:00'),(105,'Тут что-то не так.','l4GTZ2Q1zQ0.jpg','2013-03-14 17:53:47','2013-03-14 17:53:47',9,3,'ilikes_101156','2013-03-13 20:00:00','2013-03-14 08:00:00'),(106,'Тут что-то не так.','l4GTZ2Q1zQ0.jpg','2013-03-14 17:53:48','2013-03-14 17:53:48',9,3,'ilikes_101156','2013-03-13 20:00:00','2013-03-14 08:00:00'),(107,'','tFPkll2anCc.jpg','2013-03-14 17:53:59','2013-03-14 17:53:59',2,2,'mudakoff_14866456','2013-03-13 20:00:00','2013-03-14 08:00:00'),(108,'','tFPkll2anCc.jpg','2013-03-14 17:54:05','2013-03-14 17:54:05',2,3,'mudakoff_14866456','2013-03-13 20:00:00','2013-03-14 08:00:00'),(109,'','_xQuYWYiARo.jpg','2013-03-14 17:54:19','2013-03-14 17:54:19',20,2,'aqvamania_3002','2013-03-13 20:00:00','2013-03-14 08:00:00'),(110,'','znoWaBCXlz0.jpg','2013-03-14 17:54:30','2013-03-14 17:54:30',6,1,'pizdato_0_31941','2013-03-13 20:00:00','2013-03-14 08:00:00'),(111,'','znoWaBCXlz0.jpg','2013-03-14 17:54:38','2013-03-14 17:54:38',6,3,'pizdato_0_31941','2013-03-13 20:00:00','2013-03-14 08:00:00'),(112,'','-v18QtkOP3A.jpg','2013-03-14 17:54:54','2013-03-14 17:54:54',19,3,'pozitiv_253800','2013-03-13 20:00:00','2013-03-14 08:00:00'),(113,'Котэ-наркотэ','yMBaS9Nu3YY.jpg','2013-03-14 17:55:11','2013-03-14 17:55:11',37,2,'flike_49057','2013-03-13 20:00:00','2013-03-14 08:00:00'),(114,'Котэ-наркотэ','yMBaS9Nu3YY.jpg','2013-03-14 17:55:17','2013-03-14 17:55:17',37,1,'flike_49057','2013-03-13 20:00:00','2013-03-14 08:00:00'),(115,'Котэ-наркотэ','yMBaS9Nu3YY.jpg','2013-03-14 17:55:23','2013-03-14 17:55:23',37,3,'flike_49057','2013-03-13 20:00:00','2013-03-14 08:00:00'),(140,'','PAvuryI46mk.jpg','2013-03-15 10:43:34','2013-03-15 10:43:34',29,1,'angab_27749','2013-03-14 20:00:00','2013-03-15 08:00:00');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `public_and_posts`
--

DROP TABLE IF EXISTS `public_and_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `public_and_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `public_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `post_date_time` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `public_and_posts`
--

LOCK TABLES `public_and_posts` WRITE;
/*!40000 ALTER TABLE `public_and_posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `public_and_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `public_invites`
--

DROP TABLE IF EXISTS `public_invites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `public_invites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `public_id` int(11) DEFAULT NULL,
  `vk_user_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `public_invites`
--

LOCK TABLES `public_invites` WRITE;
/*!40000 ALTER TABLE `public_invites` DISABLE KEYS */;
/*!40000 ALTER TABLE `public_invites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publics`
--

DROP TABLE IF EXISTS `publics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vk_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `closed` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publics`
--

LOCK TABLES `publics` WRITE;
/*!40000 ALTER TABLE `publics` DISABLE KEYS */;
INSERT INTO `publics` VALUES (1,'JOkie JOkie','jokie_umor','48529852','41TJ3Vb6K5g.jpg',1,'2013-03-14 05:57:44','2013-03-14 05:57:44',2),(2,'Любители похихикать','i.love.humor','50988585','mIakQGFtU9g.jpg',1,'2013-03-14 07:01:39','2013-03-14 12:03:15',2),(3,'Юмор для БРО','umor_dlya_bro','50988665','whrx246lTdA.jpg',1,'2013-03-14 07:03:26','2013-03-14 12:03:34',2);
/*!40000 ALTER TABLE `publics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scheduled_posts`
--

DROP TABLE IF EXISTS `scheduled_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scheduled_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `schedule_id` int(11) DEFAULT NULL,
  `post_date_time` datetime DEFAULT NULL,
  `published` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `vk_post_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scheduled_posts`
--

LOCK TABLES `scheduled_posts` WRITE;
/*!40000 ALTER TABLE `scheduled_posts` DISABLE KEYS */;
INSERT INTO `scheduled_posts` VALUES (74,74,1,'2013-03-14 08:00:00',0,'2013-03-14 17:35:42','2013-03-14 17:35:42',NULL),(75,75,1,'2013-03-14 08:00:00',0,'2013-03-14 17:36:03','2013-03-14 17:36:03',NULL),(76,76,1,'2013-03-14 08:00:00',0,'2013-03-14 17:36:12','2013-03-14 17:36:12',NULL),(77,77,1,'2013-03-14 08:00:00',0,'2013-03-14 17:36:34','2013-03-14 17:36:34',NULL),(78,78,1,'2013-03-14 08:00:00',0,'2013-03-14 17:37:31','2013-03-14 17:37:31',NULL),(79,79,1,'2013-03-14 08:00:00',0,'2013-03-14 17:37:57','2013-03-14 17:37:57',NULL),(80,80,1,'2013-03-14 08:00:00',0,'2013-03-14 17:38:53','2013-03-14 17:38:53',NULL),(81,81,1,'2013-03-14 08:00:00',0,'2013-03-14 17:39:50','2013-03-14 17:39:50',NULL),(84,84,2,'2013-03-14 08:00:00',0,'2013-03-14 17:40:59','2013-03-14 17:40:59',NULL),(85,85,3,'2013-03-14 08:00:00',0,'2013-03-14 17:42:26','2013-03-14 17:42:26',NULL),(86,86,2,'2013-03-14 08:00:00',0,'2013-03-14 17:42:36','2013-03-14 17:42:36',NULL),(87,87,3,'2013-03-14 08:00:00',0,'2013-03-14 17:42:54','2013-03-14 17:42:54',NULL),(88,88,2,'2013-03-14 08:00:00',0,'2013-03-14 17:43:13','2013-03-14 17:43:13',NULL),(89,89,2,'2013-03-14 08:00:00',0,'2013-03-14 17:43:35','2013-03-14 17:43:35',NULL),(90,90,3,'2013-03-14 08:00:00',0,'2013-03-14 17:43:45','2013-03-14 17:43:45',NULL),(91,91,1,'2013-03-14 08:00:00',0,'2013-03-14 17:43:54','2013-03-14 17:43:54',NULL),(92,92,3,'2013-03-14 08:00:00',0,'2013-03-14 17:44:24','2013-03-14 17:44:24',NULL),(93,93,3,'2013-03-14 08:00:00',0,'2013-03-14 17:44:33','2013-03-14 17:44:33',NULL),(94,94,1,'2013-03-14 08:00:00',0,'2013-03-14 17:44:41','2013-03-14 17:44:41',NULL),(95,95,3,'2013-03-14 08:00:00',0,'2013-03-14 17:50:00','2013-03-14 17:50:00',NULL),(96,96,2,'2013-03-14 08:00:00',0,'2013-03-14 17:50:13','2013-03-14 17:50:13',NULL),(97,97,2,'2013-03-14 08:00:00',0,'2013-03-14 17:50:54','2013-03-14 17:50:54',NULL),(98,98,1,'2013-03-14 08:00:00',0,'2013-03-14 17:51:05','2013-03-14 17:51:05',NULL),(99,99,2,'2013-03-14 08:00:00',0,'2013-03-14 17:51:15','2013-03-14 17:51:15',NULL),(100,100,3,'2013-03-14 08:00:00',0,'2013-03-14 17:51:27','2013-03-14 17:51:27',NULL),(101,101,1,'2013-03-14 08:00:00',0,'2013-03-14 17:51:40','2013-03-14 17:51:40',NULL),(102,102,3,'2013-03-14 08:00:00',0,'2013-03-14 17:52:47','2013-03-14 17:52:47',NULL),(103,103,3,'2013-03-14 08:00:00',0,'2013-03-14 17:53:28','2013-03-14 17:53:28',NULL),(104,104,2,'2013-03-14 08:00:00',0,'2013-03-14 17:53:41','2013-03-14 17:53:41',NULL),(105,105,3,'2013-03-14 08:00:00',0,'2013-03-14 17:53:47','2013-03-14 17:53:47',NULL),(106,106,3,'2013-03-14 08:00:00',0,'2013-03-14 17:53:48','2013-03-14 17:53:48',NULL),(107,107,2,'2013-03-14 08:00:00',0,'2013-03-14 17:53:59','2013-03-14 17:53:59',NULL),(108,108,3,'2013-03-14 08:00:00',0,'2013-03-14 17:54:05','2013-03-14 17:54:05',NULL),(109,109,2,'2013-03-14 08:00:00',0,'2013-03-14 17:54:19','2013-03-14 17:54:19',NULL),(110,110,1,'2013-03-14 08:00:00',0,'2013-03-14 17:54:30','2013-03-14 17:54:30',NULL),(111,111,3,'2013-03-14 08:00:00',0,'2013-03-14 17:54:38','2013-03-14 17:54:38',NULL),(112,112,3,'2013-03-14 08:00:00',0,'2013-03-14 17:54:54','2013-03-14 17:54:54',NULL),(113,113,2,'2013-03-14 08:00:00',0,'2013-03-14 17:55:11','2013-03-14 17:55:11',NULL),(114,114,1,'2013-03-14 08:00:00',0,'2013-03-14 17:55:17','2013-03-14 17:55:17',NULL),(115,115,3,'2013-03-14 08:00:00',0,'2013-03-14 17:55:23','2013-03-14 17:55:23',NULL),(140,140,4,'2013-03-15 08:00:00',0,'2013-03-15 10:43:34','2013-03-15 10:43:34',NULL);
/*!40000 ALTER TABLE `scheduled_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedules`
--

DROP TABLE IF EXISTS `schedules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `public_id` int(11) DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedules`
--

LOCK TABLES `schedules` WRITE;
/*!40000 ALTER TABLE `schedules` DISABLE KEYS */;
INSERT INTO `schedules` VALUES (1,1,'2013-03-13 20:00:00','2013-03-14 07:08:12','2013-03-14 07:08:12'),(2,2,'2013-03-13 20:00:00','2013-03-14 07:09:26','2013-03-14 07:09:26'),(3,3,'2013-03-13 20:00:00','2013-03-14 07:09:40','2013-03-14 07:09:40'),(4,1,'2013-03-14 20:00:00','2013-03-15 10:43:34','2013-03-15 10:43:34');
/*!40000 ALTER TABLE `schedules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20130205154544'),('20130205154649'),('20130205181851'),('20130206105351'),('20130206182436'),('20130207185031'),('20130207194947'),('20130211180010'),('20130211180248'),('20130211180400'),('20130211180813'),('20130211181238'),('20130211181547'),('20130211181641'),('20130211181741'),('20130212194421'),('20130212200814'),('20130217161800'),('20130217172823'),('20130217174052'),('20130217175353'),('20130217180337'),('20130220175929'),('20130220180055'),('20130220180506'),('20130220181059'),('20130226053447'),('20130226063414'),('20130226070127');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-03-15 20:19:33
