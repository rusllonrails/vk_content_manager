class CreateVkPosts < ActiveRecord::Migration
  def change
    create_table :vk_posts do |t|
      t.string :text
      t.string :photo

      t.timestamps
    end
  end
end
