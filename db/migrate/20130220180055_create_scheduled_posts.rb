class CreateScheduledPosts < ActiveRecord::Migration
  def change
    create_table :scheduled_posts do |t|
      t.integer :post_id
      t.integer :schedule_id
      t.datetime :post_date_time
      t.boolean :published

      t.timestamps
    end
  end
end
