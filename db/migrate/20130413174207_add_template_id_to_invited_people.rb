class AddTemplateIdToInvitedPeople < ActiveRecord::Migration
  def change
    add_column :invited_people, :template_id, :integer
  end
end
