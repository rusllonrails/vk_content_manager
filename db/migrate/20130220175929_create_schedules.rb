class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.integer :public_id
      t.datetime :publish_date

      t.timestamps
    end
  end
end
