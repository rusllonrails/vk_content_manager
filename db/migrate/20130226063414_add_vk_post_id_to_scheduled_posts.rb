class AddVkPostIdToScheduledPosts < ActiveRecord::Migration
  def change
    add_column :scheduled_posts, :vk_post_id, :integer
  end
end
