class RemoveGidFromPublics < ActiveRecord::Migration
  def up
    remove_column :publics, :gid
  end

  def down
    add_column :publics, :gid, :integer
  end
end
