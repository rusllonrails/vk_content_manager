class RenameSomeTables < ActiveRecord::Migration
  def change
  	rename_table :vk_own_publics, :publics
  	rename_table :sim_cards, :accounts
  	rename_table :vk_posts, :posts
  end
end
