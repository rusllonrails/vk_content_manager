class AddBlockedToSimCards < ActiveRecord::Migration
  def change
    add_column :sim_cards, :blocked, :boolean
  end
end
