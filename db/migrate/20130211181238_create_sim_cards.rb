class CreateSimCards < ActiveRecord::Migration
  def change
    create_table :sim_cards do |t|
      t.string :phone

      t.timestamps
    end
  end
end
