class ChangePasswordFieldTypeInAccounts < ActiveRecord::Migration
  def change
  	change_column :accounts, :password, :string
  end
end
