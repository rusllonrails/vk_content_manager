class AddFieldsToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :first_name, :string
    add_column :accounts, :last_name, :string
    add_column :accounts, :avatar, :string
    add_column :accounts, :vk_id, :string
    add_column :accounts, :email, :string
    add_column :accounts, :password, :string
  end
end
