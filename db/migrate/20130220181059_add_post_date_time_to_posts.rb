class AddPostDateTimeToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :post_date_time, :datetime
  end
end
