class AddErrorCodeToInvitedPeople < ActiveRecord::Migration
  def change
    add_column :invited_people, :error_code, :string
  end
end
