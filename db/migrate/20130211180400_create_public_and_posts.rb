class CreatePublicAndPosts < ActiveRecord::Migration
  def change
    create_table :public_and_posts do |t|
      t.integer :public_id
      t.integer :post_id
      t.datetime :post_date_time

      t.timestamps
    end
  end
end
