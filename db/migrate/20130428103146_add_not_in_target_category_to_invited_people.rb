class AddNotInTargetCategoryToInvitedPeople < ActiveRecord::Migration
  def change
    add_column :invited_people, :not_in_target_category, :boolean, default: false
  end
end
