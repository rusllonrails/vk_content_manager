class AddAccessTokenToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :access_token, :string
  end
end
