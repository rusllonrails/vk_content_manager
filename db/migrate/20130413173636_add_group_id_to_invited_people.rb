class AddGroupIdToInvitedPeople < ActiveRecord::Migration
  def change
  	add_column :invited_people, :public_id, :integer
    add_column :invited_people, :group_id, :integer
    add_column :invited_people, :invited, :boolean
    add_column :invited_people, :in_black_list, :boolean
    add_column :invited_people, :is_member, :boolean
  end
end
