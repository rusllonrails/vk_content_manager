class AddAccountIdToPublics < ActiveRecord::Migration
  def change
    add_column :publics, :account_id, :integer
  end
end
