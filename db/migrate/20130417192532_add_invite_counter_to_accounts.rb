class AddInviteCounterToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :invite_counter, :integer, default: 0
  end
end
