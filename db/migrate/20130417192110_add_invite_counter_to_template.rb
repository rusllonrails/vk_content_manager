class AddInviteCounterToTemplate < ActiveRecord::Migration
  def change
    add_column :templates, :invite_counter, :integer, default: 0
  end
end
