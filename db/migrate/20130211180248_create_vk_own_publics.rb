class CreateVkOwnPublics < ActiveRecord::Migration
  def change
    create_table :vk_own_publics do |t|
      t.string :name
      t.string :short_name
      t.string :vk_id
      t.string :avatar
      t.boolean :closed
      t.integer :gid

      t.timestamps
    end
  end
end
