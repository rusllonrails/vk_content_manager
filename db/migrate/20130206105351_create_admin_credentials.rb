class CreateAdminCredentials < ActiveRecord::Migration
  def change
    create_table :admin_credentials do |t|
      t.integer :admin_id
      t.string :vk_code

      t.timestamps
    end
  end
end
