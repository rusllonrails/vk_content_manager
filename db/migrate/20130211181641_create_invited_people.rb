class CreateInvitedPeople < ActiveRecord::Migration
  def change
    create_table :invited_people do |t|
      t.integer :vk_id

      t.timestamps
    end
  end
end
