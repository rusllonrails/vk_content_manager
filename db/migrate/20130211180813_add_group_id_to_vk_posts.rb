class AddGroupIdToVkPosts < ActiveRecord::Migration
  def change
    add_column :vk_posts, :group_id, :integer
  end
end
