class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :name
      t.string :short_name
      t.string :vk_id
      t.string :avatar

      t.timestamps
    end
  end
end
