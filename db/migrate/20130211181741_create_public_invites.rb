class CreatePublicInvites < ActiveRecord::Migration
  def change
    create_table :public_invites do |t|
      t.integer :public_id
      t.integer :vk_user_id

      t.timestamps
    end
  end
end
