class ChangePostDateTimeInPosts < ActiveRecord::Migration
  def change
    change_column :posts, :post_date_time, :string
  end
end
