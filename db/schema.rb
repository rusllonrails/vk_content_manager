# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130506182758) do

  create_table "accounts", :force => true do |t|
    t.string   "phone"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.boolean  "blocked"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "avatar"
    t.string   "vk_id"
    t.string   "password"
    t.string   "access_token"
    t.integer  "invite_counter", :default => 0
  end

  create_table "admin_credentials", :force => true do |t|
    t.integer  "admin_id"
    t.string   "vk_code"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "admins", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "avatar"
  end

  add_index "admins", ["email"], :name => "index_admins_on_email", :unique => true
  add_index "admins", ["reset_password_token"], :name => "index_admins_on_reset_password_token", :unique => true

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "groups", :force => true do |t|
    t.string   "name"
    t.string   "short_name"
    t.string   "vk_id"
    t.string   "avatar"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.boolean  "closed"
    t.integer  "gid"
  end

  create_table "invited_people", :force => true do |t|
    t.integer  "vk_id"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.integer  "public_id"
    t.integer  "group_id"
    t.boolean  "invited"
    t.boolean  "in_black_list"
    t.boolean  "is_member"
    t.integer  "template_id"
    t.string   "error_code"
    t.boolean  "not_in_target_category", :default => false
  end

  create_table "posts", :force => true do |t|
    t.string   "text"
    t.string   "photo"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.integer  "group_id"
    t.integer  "public_id"
    t.string   "vk_post_id"
    t.datetime "publish_date"
    t.string   "post_date_time"
  end

  create_table "public_and_posts", :force => true do |t|
    t.integer  "public_id"
    t.integer  "post_id"
    t.datetime "post_date_time"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "public_invites", :force => true do |t|
    t.integer  "public_id"
    t.integer  "vk_user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "publics", :force => true do |t|
    t.string   "name"
    t.string   "short_name"
    t.string   "vk_id"
    t.string   "avatar"
    t.boolean  "closed"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "account_id"
  end

  create_table "scheduled_posts", :force => true do |t|
    t.integer  "post_id"
    t.integer  "schedule_id"
    t.datetime "post_date_time"
    t.boolean  "published"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.integer  "vk_post_id"
  end

  create_table "schedules", :force => true do |t|
    t.integer  "public_id"
    t.datetime "publish_date"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "templates", :force => true do |t|
    t.string   "title"
    t.text     "content"
    t.string   "image"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.integer  "invite_counter", :default => 0
  end

end
